---
title: Guide étudiants du CRIUGM
author: Représentants étudiants du CRIUGM
date: Années 2015-2016
geometry: margin=2.2cm
---

# Université de Montréal

Dans la gestion de votre dossier et les communications avec vous, l’Université de Montréal utilise une terminologie qu’il est important de bien maîtriser. 
Pour vous permettre de mieux comprendre ce qui suit, voici quelques-uns des termes les plus fréquemment employés.

**UdeM :** abréviation courante pour l’Université de Montréal.

**Code permanent :** numéro qui vous est attribué par l’Université de Montréal lors de votre inscription. 
Il comprend les 3 premières lettres de votre nom, la première de votre prénom et 8 chiffres.
Le code permanent permet de vous identifier auprès des services institutionnels et gouvernementaux.

**UNIP :** numéro d’identification personnel.
Un UNIP temporaire vous est attribué après votre inscription.
Il vous permet de vous brancher au service de courriel ainsi qu’à votre portail UdeM.

**Portail UdeM :** Portail d’accueil qui vous permet d’avoir accès à votre dossier, vos courriels, votre
dossier de lecteur (bibliothèque), vos cours, vos données personnelles, etc.


## Démarches par rapport à l’Université

### Inscription universitaire

À l’Université de Montréal, c’est la **Faculté des Études Supérieures et Postdoctorales** (FESP) qui gère les inscriptions des étudiants à la maîtrise, au doctorat et au postdoctorat.
L’université offre un portail dédié à l’adresse suivante [https://admission.umontreal.ca/](https://admission.umontreal.ca/).

**Attention aux dates limites d’admission.** 
Le plus souvent début février pour l’automne suivant et mi-septembre pour les sessions d’hiver.
Pour les *étudiants étrangers*, il est stipulé sur le site de l’UdeM \og{}que les étudiants étrangers doivent compter de 3 à 6 mois pour remplir toutes les formalités relatives à leur séjour d'études. Si ces formalités ne sont pas remplies à temps, vous devrez déposer une nouvelle demande d'admission ultérieurement \fg{}.

Vous aurez à remplir un formulaire sur Internet et fournir quelques pièces justificatives qui dépendent notamment de votre statut (citoyen, résident permanent ou étudiant étranger).
Par exemple, les étudiants étrangers devront fournir une copie du permis d’étude ou de travail ainsi qu’une copie de leur passeport.

Finalement, il faudra s’acquitter (ou faire payer par votre laboratoire d’accueil pour les postdoctorants) de différents frais [voir le site de l’UdeM](http://www.etudes.umontreal.ca/payer-etudes/droit-scolarite.html)[^1] :

[^1]: [http://www.etudes.umontreal.ca/payer-etudes/droit-scolarite.html](http://www.etudes.umontreal.ca/payer-etudes/droit-scolarite.html)

- *frais de scolarité* pour la maîtrise et le doctorat,
- *frais de gestion* (124,95$ par trimestre à temps plein),
- *frais d’accès aux services aux étudiants* (128$ par trimestre à temps plein),
- *frais de soutien aux bibliothèques* (24,30$ par trimestre à temps plein), 
- *frais de soutien aux technologies de l’enseignement* (3$ par trimestre à temps plein)
- *frais de droits d’auteur* (3,30$ par trimestre à temps plein)
- *frais d’adhésion aux associations étudiantes* (selon le niveau universitaire)

### Carte d’étudiant
 
Les étudiants à la maîtrise et au doctorat recevront automatiquement une carte d’étudiant, alors qu’elle sera optionnelle pour les stagiaires postdoctoraux (voir le site Internet de l’UdeM [http://www.carte.umontreal.ca/](http://www.carte.umontreal.ca/)).

La carte peut se récupérer au *Pavillon J.-A.-DeSève, 2332, boul. Édouard-Montpetit*, au Rez-de-chaussée (Métro Édouard-Montpetit) ou par envoi postal si la demande est complétée en ligne.
Les jours d’ouverture varient selon le moment de l’année : [http://www.carte.umontreal.ca/horaire.html](http://www.carte.umontreal.ca/horaire.html).

Pour une demande en ligne, il vous sera demandé a minima ([voir le site Internet](http://www.carte.umontreal.ca/soumettre-en-ligne.html)[^2]):

[^2]: [http://www.carte.umontreal.ca/soumettre-en-ligne.html](http://www.carte.umontreal.ca/soumettre-en-ligne.html)

- votre matricule
- une photo 
- une signature numérisée

Pour une demande physique, vous devez vous présenter au centre avec les documents suivants :

- *pièce d'identité valide*
- *matricule*
- *attestation de résidence permanente* ou *permis étudiant* ou *permis de travail*

Pour les *stagiaires postdoctoraux*, il faut se renseigner auprès de la [FESP](http://www.fes.umontreal.ca/stages_postdoc/inscription.htmla)[^3].

[^3]: [http://www.fes.umontreal.ca/stages_postdoc/inscription.html](http://www.fes.umontreal.ca/stages_postdoc/inscription.html)

### Code permanent et UNIP

Une fois inscrit(e) à la FESP, vous recevrez par courrier une confirmation d’inscription du Registrariat.
Vous y trouverez votre code permanent ainsi que la procédure à suivre pour obtenir un numéro d’identification personnel (UNIP); ce dernier vous permettra d’accéder à votre portail sur le site de l’Université de Montréal.

### Adresse courriel
Une adresse de courriel institutionnelle est généralement sous le format : `prénom.nom@umontreal.ca`.
Les étudiants à la maîtrise et au doctorat bénéficieront automatiquement d’une adresse de courriel auprès de l’Université. 

En revanche, les étudiants au postdoctorat devront en faire la demande auprès des services de la Direction générale des technologies de l’information et de la communication (DGTIC).
Vous aurez besoin de votre code permanent et de votre UNIP. 
La demande peut se faire directement auprès de la DGTIC au 514-343-6111 poste : 1- 7288.


## Services accessibles

En tant que membre de l’UdeM, vous aurez accès aux différents services offerts à la communauté universitaire dont voici ci-dessous quelques exemples.

### Bibliothèques
Les étudiants à la maîtrise et doctorat ont un accès aux ressources des bibliothèques physiques et numériques de l’UdeM.
Cet accès comprend :

- *consultation et emprunt de livres*
- accès aux *articles scientifiques* de nombreuses bases de données
- *informations et formations à la documentation*
- accès aux *zones de travail* dans les différentes bibliothèques du campus

Les stagiaires postdoctoraux bénéficient des avantages accordés aux professeurs et aux étudiants de 3\up{e} cycle par la Direction des bibliothèques. 
Certains services ou accès supplémentaires s’ajoutent alors à la liste mentionnée ci-dessus.

### Informatique
Vous aurez également accès aux différents services informatiques de l’UdeM (DGTIC : [http://www.dgtic.umontreal.ca/](http://www.dgtic.umontreal.ca/)):

- *Adresse courriel* et gestionnaire de messagerie
- *Studium* (Moodle, gestionnaire de cours en ligne)
- *Synchro* (gestion de votre dossier et votre scolarité)
- *Équipement informatique* (utilisation, prêt, etc.)
- *Logiciels* (accès, prix réduits, etc.)
- ...

### Services aux étudiants (SAÉ)
Les étudiants à la maîtrise et au doctorat bénéficieront des services aux étudiants sur présentation de leur carte étudiante (sports, activités culturelles, services de santé, logement etc.).

Les stagiaires postdoctoraux devront acquitter les frais requis qui s’élèvent à environ 130$ par trimestre.
Ces frais sont payables aux SAÉ, Pavillon J-A de Sève au 5\up{e} étage, bureau C-5530-1. 
Vous pourrez par la suite vous présenter auprès des : Services aux Étudiants (SAÉ) au Pavillon J-A de Sève pour obtenir une carte de l’Université de Montréal.

### Assurance santé
Pour les citoyens et résidents canadiens, l’adhésion aux associations étudiantes permet l’accès à des assurances santé collectives médicale et dentaire. 
Celles-ci peuvent être souscrites auprès de la [FAÉCUM](http://www.faecum.qc.ca/services/assurances-aseq)[^4] (Fédération des associations étudiantes du campus de l'Université de Montréal) ou de l’[ASEQ](http://www.ageefep.qc.ca/)[^5] (Association générale des étudiants et étudiantes de la Faculté de l'éducation permanente de l'Université de Montréal).

[^4]: [http://www.faecum.qc.ca/services/assurances-aseq](http://www.faecum.qc.ca/services/assurances-aseq)
[^5]: [http://www.ageefep.qc.ca/](http://www.ageefep.qc.ca/)

Pour les étudiants étrangers, il existe plusieurs solutions selon votre pays de provenance (voir les ententes France-Québec, pour les Français) et plusieurs offres de l’[UdeM](http://www.bei.umontreal.ca/bei/ass_couverture.htm)[^6].
Il est important de **se renseigner avant votre départ pour le Québec**, car vous pourriez avoir besoin de justificatifs fournis uniquement dans votre pays d’origine.

[^6]: [http://www.bei.umontreal.ca/bei/ass_couverture.htm](http://www.bei.umontreal.ca/bei/ass_couverture.htm)
