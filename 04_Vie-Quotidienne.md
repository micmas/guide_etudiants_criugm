---
title: Vie quotidienne à Montréal et aux alentours du CRIUGM
author: Maxime Montembault
date: 2015/04/20
---

# Vie quotidienne à Montréal

## Logement

À Montréal, les agences immobilières sont relativement peu nombreuses.
Un des meilleurs moyens de trouver un logement est de parcourir les rues de la ville.
En général, les appartements à louer affichent une pancarte \og{} À louer \fg{} rouge.
Prenez les numéros de téléphone de tous les logements susceptibles de vous intéresser, appelez et prenez rendez-vous pour les visiter.

Un autre moyen consiste à consulter des sites affichant des locations :

- Kijiji: [http://montreal.kijiji.ca/](http://montreal.kijiji.ca/)
- Craigslist: [http://montreal.en.craigslist.ca/apa/](http://montreal.en.craigslist.ca/apa/)
- Lappart: [http://lappart.info/](http://lappart.info/)
- Voir.ca: [http://www2.voir.ca/aLouer/](http://www2.voir.ca/aLouer/)
- Bureau du logement de l’UdeM: [http://www.logement.umontreal.ca](http://www.logement.umontreal.ca) 

Le système québécois désigne la grandeur des appartements selon le nombre de pièces principales (sans la cuisine) et la présence ou non d’une salle de bain qui est notée par un "1/2".
Ainsi, les dénomiations suivantes correspondent par exemple à :

- 1 1/2 : studio à une pièce plus la salle de bain 
- 2 1/2 : salon avec chambre séparée plus la salle de bain
- 3 1/2 : salon avec une chambre et un salon (ou deux chambres parfois) plus la salle de bain
- ...

Le prix des appartements est très variable selon la grandeur, l’âge et l’état de l’appartement, les services offerts, le quartier considéré, etc. 
Par exemple, le quartier du Plateau-Mont-Royal est reconnu comme étant beaucoup plus onéreux que celui de Côte-des-Neiges. 
N’hésitez pas à vous renseigner !

Pour vous informer sur les modalités de signature de bail, vos droits et vos obligations (qui peuvent différer par rapport à votre pays d’origine si vous êtes étranger), rendez-vous sur le site de la [Régie du logement du Québec](http://www.rdl.gouv.qc.ca/fr/accueil/accueil.asp)[^7].
Les baux sont en général signés pour un an et la plupart commencent le 1er juillet.

[^7]: [http://www.rdl.gouv.qc.ca/fr/accueil/accueil.asp](http://www.rdl.gouv.qc.ca/fr/accueil/accueil.asp) 

Pour les étudiants de l’Université de Montréal, vous pouvez rencontrer les gens du [Bureau du logement](http://www.logement.umontreal.ca)[^8] hors campus de l’université de Montréal, qui se feront un plaisir de répondre à vos questions et vous fournir des conseils.
Plusieurs  outils sont disponibles sur place pour faciliter votre recherche.
Sur le site web, vous pouvez également trouver une banque de logements disponibles hors campus.

[^8]: [http://www.logement.umontreal.ca](http://www.logement.umontreal.ca)

Lors de la location d’un appartement, il est important de considérer différents facteurs.
Par exemple, il est possible de louer des appartements non meublés, semi-meublés (qui comprennent la majorité du temps un frigidaire et un four), ou meublés.
Certains appartements sont également chauffés (prix du chauffage inclus) et/ou éclairés (prix de l’électricité inclus).
Vous trouverez également la mention "demi sous-sol" pour les appartements qui sont à demi enterrés dans le sol, c’est-à-dire dont les fenêtres sont au niveau du sol. 
Ces appartements sont moins chers, mais beaucoup moins lumineux.
Finalement, de nombreux bâtiments contiennent une buanderie (laveuses et sécheuses), ce qui peut être très pratique.


## Stationnement

Pour en apprendre plus sur le stationnement sur la rue dans la ville de Montréal, visitez le site de la [ville Montréal](http://ville.montreal.qc.ca/portal/page?_pageid=8957,99645826&_dad=portal&_schema=PORTAL)[^9].

[^9]: [http://ville.montreal.qc.ca/portal/page?_pageid=8957,99645826&_dad=portal&_schema=PORTAL](http://ville.montreal.qc.ca/portal/page?_pageid=8957,99645826&_dad=portal&_schema=PORTAL)

Au CRIUGM, en plus des vignettes de stationnement disponibles pour le stationnement, il y a possibilité de se stationner dans les rues à proximité.
Par exemple, les rues *Cedar Crescent*, *Roslyn*, *Stanley Weir*, *Michel Bibeau* et *Miller* situées près du Chemin Queen Mary comptent souvent plusieurs places disponibles.
Faites tout de même attention aux restrictions qui s’appliquent du 1er avril au 1er décembre et qui interdisent le stationnement durant certaines périodes (celles-ci sont inscrites sur des panneaux sur le côté de la rue).


## Courses et sorties

Voici une sélection d’endroits où sortir aux alentours du CRIUGM (dans le quartier Côte-Des-Neiges).
Dans plusieurs commerces de ce quartier, des rabais s’appliquent sur vos achats sur présentation de votre carte étudiante de l’Université de Montréal ou de celle du CRIUGM.

### Endroits pour faire les courses

- *Marché public Jean-Brillant / métro Côte-des-Neiges* :
    - Les meilleurs prix pour les fruits et légumes frais!
    - Adresse : Angle Jean-Brillant / Côte-des-Neiges
    - En face du métro Côte-des-Neiges
    - Ouvert de la fin mars jusqu’à Noël

- *Exofruits* :
    - Épicerie fine et magasin de fruits & légumes
    - Adresse : 5192 Chemin de la Côte-des-Neiges

- *Épicerie Métro* :
    - Supermarché
    - Adresses :
        - 5150 Chemin de la Côte-des-Neiges
        - 5201 Chemin Queen Mary

### Endroits pour sortir et prendre un verre

- *Les tontons flingueurs* :
    - Bistro/brasserie française sympathique
    - Adresse: 5190, chemin de la Côte-des-Neiges

- *Pub le McCarolds* :
    - Le pub idéal pour les 5 à 7!
    - Adresse: 5400 chemin de la Côte-des-Neiges

- *Tabasco bar* :
    - Bar avec table de billard et terrasse ensoleillée l’été
    - Adresse: 5414 Avenue Gatineau

- *Librairie Olivieri*
    - Librairie/Bistro offrant une terrasse en arrière cours l’été
    - Adresse : 5219 Chemin de la Côte-des-Neiges

### Cafés

- *Java U Café* :
    - Adresse : 5620 Chemin de la Côte-des-Neiges

- *Starbucks* :
    - La franchise internationale bien connue
    - Adresses :
        -  5300 Chemin de la Côte-des-Neiges
        - 5484 Chemin de la  Côte-des-Neiges

- *Brûlerie St Denis* :
    - Différents cafés et boissons chaudes et sucreries. Repas de type de brasserie.
    - Adresse : 5252 Chemin de la Côte-des-Neige

- *Second Cup* :
    - Cafe franchisé
    - Adresse : 5206 Chemin de la Côtes des Neiges,

### Restaurants :

Une des spécialités du quartier sont les soupes tonkinoises qui sont disponibles dans plusieurs restaurants sur le chemin de la Côte-des-Neiges.

- *Brit & Chips* :
    - Un des meilleurs fish'n chips de Montréal !
    - Adresse : 5536 Chemin de la Côte-des-Neiges

- *Tais toi et mange* :
    - Adresse : 5153 Chemin de la Côte-des-Neiges

-  *Restaurant Nguyen Phi* :
     - Spécialiste des soupes tonkinoises
- Adresse : 6260, ch de la Côte-des-Neiges

- *Allô! Mon coco* :
    - Des brunchs très copieux et bons
    - Adresse : 5685 Chemin de la Côte-des-Neiges

- *Les tontons flingueurs* :
    - Brasserie française
    - Adresse: 5190, chemin de la Côte-des-Neiges



