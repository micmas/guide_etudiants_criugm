---
title: Formalités administratives
author: Bérengère Houzé
---

# Étudiants étrangers

## Citoyenneté et Immigration Canada

### Permis d’étude – maîtrise, doctorat

Dans le cas où vous souhaitez suivre une formation menant à un diplôme d’une des universités du Québec, vous avez besoin de l’avis d’admission de l’université que vous souhaitez fréquenter, du certificat d’acceptation du Québec (CAQ), d’un permis d’étude délivré par le gouvernement fédéral du Canada et dans certains cas, d’un visa d’entrée au Canada. 
Cet enchainement de procédures doit se faire **avant** votre arrivée au Québec afin que vous ayez les papiers nécessaires en main à votre arrivée sur le sol québécois.

*Note :* Si vous venez seulement dans le cadre d’un échange de moins de 6 mois, vous n’aurez pas besoin du CAQ ni du permis d’études, le permis de visiteur suffit (sauf si vous devez toucher une rémunération).

#### Certificat de sélection du Québec – CAQ

Le CAQ doit être demandé au ministère de l’Immigration du Québec pour tout ressortissant étranger. Vous pouvez faire une demande en ligne [ici](http://www.immigration-quebec.gouv.qc.ca/fr/services/caq-electronique/index.html)[^10]; vous y trouverez également toute la documentation nécessaire.
À noter qu’il vous sera demandé de prouver que vous aurez les ressources financières suffisantes pour couvrir vos besoins (preuves de fonds, octroi de bourses, autorisation de transfert de fonds…).

[^10]: [http://www.immigration-quebec.gouv.qc.ca/fr/services/caq-electronique/index.html](http://www.immigration-quebec.gouv.qc.ca/fr/services/caq-electronique/index.html)

#### Permis d’étude

Une fois votre CAQ obtenu, vous devez communiquer avec la mission diplomatique canadienne (ambassade, consulat, haut-commissariat) qui couvre le territoire où vous habitez pour obtenir votre permis d’étude. 
Vous trouverez cette information [ici](http://www.cic.gc.ca/francais/information/bureaux/demande-ou.asp)[^11]. 
La procédure de demande de permis d’étude est à envoyer par courrier au bureau des visas ou à faire en personne à la mission diplomatique dont vous dépendez.

[^11]: [http://www.cic.gc.ca/francais/information/bureaux/demande-ou.asp](http://www.cic.gc.ca/francais/information/bureaux/demande-ou.asp)

Documents requis :

- Passeport valide 
- Six photos format passeport
- Avis d'admission de l'Université (vous pouvez imprimer l'avis d'admission définitive ou conditionnelle disponible sur Centre étudiant, pour entamer le plus vite possible vos formalités d’immigration)
- Certificat d'acceptation du Québec (selon le cas, ce certificat prend la forme d'un document envoyé à l'étudiant par les services d'immigration du Québec ou d'une dépêche expédiée directement à la mission diplomatique canadienne où l'étudiant présente sa demande de permis d'études)
- Preuves de ressources financières suffisantes
- 150$ de frais à payer

#### Visa d’entrée

Les ressortissants de certains pays ont besoin d’obtenir un visa d’entrée sur le territoire canadien. 
Vous trouverez la liste à cette adresse : [http://www.cic.gc.ca/francais/visiter/visas.asp](http://www.cic.gc.ca/francais/visiter/visas.asp).


#### Liens utiles

Pour l’Université de Montréal, vous trouverez toutes les informations nécessaires [ici](http://www.bei.umontreal.ca/)[^13].
Pour l’UQAM, c’est [ici](http://www.etudier.uqam.ca/etudiants-etrangers)[^14] et pour Concordia, [ici](https://www.concordia.ca/fr/admission/etudiants-etrangers.html)[^15].
Enfin, une fois bien arrivé, n’oubliez pas de finaliser votre inscription à l’université.

[^13]: [http://www.bei.umontreal.ca/](http://www.bei.umontreal.ca/)
[^14]: [http://www.etudier.uqam.ca/etudiants-etrangers](http://www.etudier.uqam.ca/etudiants-etrangers)
[^15]: [https://www.concordia.ca/fr/admission/etudiants-etrangers.html](https://www.concordia.ca/fr/admission/etudiants-etrangers.html)

### Permis de travail – postdocs

Le postdoctorat est reconnu comme étant un travail au Canada, c’est pourquoi vous aurez besoin d’un permis de travail délivré par le gouvernement fédéral du Canada. 
Vous trouverez toutes les informations [ici](http://www.cic.gc.ca/francais/travailler/demande-qui.asp)[^16].

[^16]: [http://www.cic.gc.ca/francais/travailler/demande-qui.asp](http://www.cic.gc.ca/francais/travailler/demande-qui.asp)

**Important :** Selon l’université dans laquelle vous allez effectuer votre stage postdoctoral ainsi que de la nature de votre financement, vous aurez soit le statut d’étudiant soit le statut de travailleur. 
Ceci est important pour la déclaration des impôts. 
Renseignez-vous auprès de votre université.


### Renouvellement de permis

Si vous avez besoin de renouveler votre permis d’étude ou de travail, vous devrez présenter une demande de prolongation au minimum 3 mois avant la fin de validité de votre permis en cours. 

Informations concernant le permis d’étude :

- Gouvernement du Québec : [http://www.immigration-quebec.gouv.qc.ca/fr/services/caq-electronique](http://www.immigration-quebec.gouv.qc.ca/fr/services/caq-electronique/index.html) 
- Gouvernement du Canada : [http://www.cic.gc.ca/francais/etudier/etudier-prolonger.asp](http://www.cic.gc.ca/francais/etudier/etudier-prolonger.asp)
- Informations supplémentaires : [http://www.cic.gc.ca/francais/travailler/prolonger.asp](http://www.cic.gc.ca/francais/travailler/prolonger.asp).

[^17]: [http://www.immigration-quebec.gouv.qc.ca/fr/services/caq-electronique/index.html](http://www.immigration-quebec.gouv.qc.ca/fr/services/caq-electronique/index.html)
[^18]: [http://www.cic.gc.ca/francais/etudier/etudier-prolonger.asp](http://www.cic.gc.ca/francais/etudier/etudier-prolonger.asp)
[^19]: [http://www.cic.gc.ca/francais/travailler/prolonger.asp](http://www.cic.gc.ca/francais/travailler/prolonger.asp)


## Service Canada

Au Canada, il est nécessaire d’avoir un numéro d’assurance sociale délivré par Service Canada afin de pouvoir travailler et être rémunéré.
Vous pouvez suivre [ce lien](http://www.servicecanada.gc.ca/fra/nas/demande/comment.shtml)[^20] pour connaître la marche à suivre.

Pour les étudiants détenteurs d’un permis d’étude, il vous est désormais possible de travailler.
Pour savoir si votre permis d’étude vous permettra d’obtenir un numéro de NAS ou si vous devez demander sa modification, vous trouverez de l’information utile [ici](http://www.bei.umontreal.ca/bei/travail_travail.htm)[^21].

[^20]: [http://www.servicecanada.gc.ca/fra/nas/demande/comment.shtml](http://www.servicecanada.gc.ca/fra/nas/demande/comment.shtml)
[^21]: [http://www.bei.umontreal.ca/bei/travail_travail.htm](http://www.bei.umontreal.ca/bei/travail_travail.htm)


## RAMQ

La RAMQ est la Régie d’Assurance Maladie du Québec.
Lorsqu’une personne vient s’établir au Québec, il existe un délai de carence pendant lequel cette personne n’est pas couverte par la RAMQ – ce qui signifie que les soins ne seront pas remboursés.
Ce délai est de maximum 3 mois.
Pour faire votre demande de souscription à la RAMQ et obtenir votre carte Soleil, toutes les informations sont [ici](http://www.ramq.gouv.qc.ca/fr/immigrants-travailleurs-etudiants-etrangers/assurance-maladie/Pages/admissiblite.aspx)[^22].
Il existe des ententes entre RAMQ et certains pays.

Pour les étudiants, il existe des couvertures médicales proposées par votre université, renseignez-vous.

[^22]: [http://www.ramq.gouv.qc.ca/fr/immigrants-travailleurs-etudiants-etrangers/assurance-maladie/Pages/admissiblite.aspx](http://www.ramq.gouv.qc.ca/fr/immigrants-travailleurs-etudiants-etrangers/assurance-maladie/Pages/admissiblite.aspx)

