TARGET=test.pdf

all:
    pandoc -s -S *.md -o test.pdf -V lang=frenchb -N --chapters --toc --toc-depth=2 --template=template.latex

    pandoc -s -S *.md -o GuideEtudiant_v02.pdf -V lang=frenchb -N --chapters --toc --toc-depth=2 --template=template.latex
