# Guide d’accueil des étudiants du CRIUGM

**Projet:** Représentant des étudiants du CRIUGM  
**Responsable:** [Guillaume Vallet](mailto:gtvallet@gmail.com)  
**Auteurs:**  
- Guillaume Vallet
- Bérengère Houzé
- Maxime Montembeault

*Version:* v1.0  
*Licence:* [Creative Common By-Nc-Sa v4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/)  
*Date:* Mars 2016

## Origine
Ce guide se veut un recueil d’informations utiles pour l’accueil des étudiants au centre, et ce quel que soit leur niveau (maîtrise, doctorat, postdoctorat).
Le centre mettait à disposition un mini-guide d’accueil à destination des postdoctorants uniquement. 
L’ancienne version n’avait pas été mise à jour depuis 2010.

## Organisation
Le guide est structuré en *quatre parties* :

1. Université de Montréal
2. Centre de recherche
3. Vie quotidienne
4. Immigration

## Structure interne

### Langage utilisé
Le guide est rédigé en **Markdown** afin de faciliter son écriture, contrôler sa mise en page et permettre l’exportation dans différents formats (pdf, html...).
Vous pouvez trouver de nombreux guides d’introduction au Markdown, par exemple [ici](http://blog.wax-o.com/2014/04/tutoriel-un-guide-pour-bien-commencer-avec-markdown/) ou [ici](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) en anglais.

### Organisation des fichiers
Chaque chapitre est rédigé dans un fichier à part dont le nom est précédé d’un index qui définira sa position dans le guide final.

### Conversion en PDF et HTML
La conversion version le format `PDF` ou `HTML` se fait grâce à [Pandoc](http://johnmacfarlane.net/pandoc/). 
La conversion en `PDF` nécessite également une installation fonctionnelle de `LaTeX` sur votre machine ([voir ici](https://fr.wikipedia.org/wiki/LaTeX)).

La commande de Pandoc pour convertir les fichiers sources en `PDF`:
```bash
pandoc -s -S *.md -o GuideEtudiantCRIUGM.pdf -V lang=frenchb -N --chapters --toc --toc-depth=2 --template=template.latex
```

La commande de Pandoc pour convertir les fichiers sources en `HTML`:
```bash
pandoc -s -S *.md -o GuideEtudiantCRIUGM.html -N --chapters --toc --toc-depth=2
```

Une version Word a également été ajouté dans les fichiers en ligne (version 1.0).
